package linpeng.adapter;

import java.util.List;

import linpeng.coolpad.R;
import linpeng.domain.Phone;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class MyGridViewAdapter extends BaseAdapter {

	private List<Phone> phones;
	private Context context;
	
	public MyGridViewAdapter(Context context,List<Phone> phones) {
		super();
		this.context=context;
		this.phones = phones;
	}

	
	
	public List<Phone> getPhones() {
		return phones;
	}



	public void setPhones(List<Phone> phones) {
		this.phones = phones;
	}



	@Override
	public int getCount() {
		return phones.size();
	}

	@Override
	public Object getItem(int position) {
		return phones.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		RelativeLayout relativeLayout;
		if(convertView!=null){
			relativeLayout=(RelativeLayout) convertView;
		}else{
			relativeLayout=(RelativeLayout) View.inflate(context, R.layout.gridview_main, null);
		}
		ImageView imageView=(ImageView) relativeLayout.findViewById(R.id.main_gridview_image);
		imageView.setImageBitmap(phones.get(position).getPhonePhoto());
		((TextView)relativeLayout.findViewById(R.id.main_gridview_text_phone_name)).setText(phones.get(position).getPhoneName());
		((TextView)relativeLayout.findViewById(R.id.main_gridview_text_phone_describe)).setText(phones.get(position).getPhoneDescribe());
		((TextView)relativeLayout.findViewById(R.id.main_gridview_text_phone_price)).setText(phones.get(position).getPhonePrice());
		
		return relativeLayout;
	}

}
