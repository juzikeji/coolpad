package linpeng.coolpad;

import java.util.ArrayList;
import java.util.List;
import linpeng.adapter.MyGridViewAdapter;
import linpeng.domain.Phone;
import linpeng.globel.MyGlobel;
import linpeng.htmlutil.GetMainGoodsService;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;

public class MainActivity extends Activity {

	private GridView gridView;
	private List<Phone> phones=new ArrayList<Phone>();
	private MyGridViewAdapter myGridViewAdapter;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		gridView=(GridView)findViewById(R.id.gridView);
		myGridViewAdapter=new MyGridViewAdapter(this, phones);
		gridView.setAdapter(myGridViewAdapter);
		new MyAsyncTaskGetData().execute("");
		gridView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				Intent intent=new Intent(MainActivity.this,DetailsActivity.class);
				intent.putExtra("index", arg2);
				startActivity(intent);
			}
		});
	}

	public class MyAsyncTaskGetData extends AsyncTask<String, String, List<Phone>>{

		@Override
		protected List<Phone> doInBackground(String... params) {
			phones=new GetMainGoodsService().getMainPhones();
			MyGlobel.phones=phones;
			myGridViewAdapter.setPhones(phones);
			return null;
		}

		@Override
		protected void onPostExecute(List<Phone> phones) {
			myGridViewAdapter.notifyDataSetChanged();
		}

	}
}
